#!/bin/bash

. 01-var.sh
. 02-funcoes.sh

# 0 desligado, 1 ligado
executa=1

if [ "${executa}" -eq 1 ]; then
    doSeparador "Instalando aplicativos..."

    # ---- app base
    apps="vim"
    apps="${apps} unzip"
    apps="${apps} unrar-free"
    apps="${apps} dirmngr"
    apps="${apps} locate"
    apps="${apps} neofetch"
    apps="${apps} tzdata"
    apps="${apps} htop"
    apps="${apps} stress"
    apps="${apps} curl"

    apt update && apt dist-upgrade -y && apt install -y ${apps} && apt -y autoremove
fi